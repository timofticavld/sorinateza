package com.example.sorinateza;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sorinateza.models.ProdusData;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class RestrictFragment extends Fragment implements RestrictRecyclerViewAdapter.ItemClickListener {
    private View rootView;
    private RecyclerView recyclerView;
    private final ArrayList<String> restrictItems = new ArrayList<>();
    private RestrictRecyclerViewAdapter adapter;

    public RestrictFragment() {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RestrictFragment newInstance() {
        return new RestrictFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.restrict_layout, container, false);
        setupRestrictList();
        setupRecyclerView();

        getActivity().setTitle("Produse interzise");
        return rootView;
    }

    private void setupRestrictList() {
        restrictItems.add("Grâu");
        restrictItems.add("Secară");
        restrictItems.add("Orz");
        restrictItems.add("Ovăz");
        restrictItems.add("Orz perlat");
        restrictItems.add("Griș");
        restrictItems.add("Cuscus");
        restrictItems.add("Pâine obișnuită");
        restrictItems.add("Pesmeți și paste");
        restrictItems.add("Conserve industriale");
        restrictItems.add("Mese gata preparate (supe, sosuri, maioneza)");
        restrictItems.add("Cofetărie");
        restrictItems.add("Budinci");
        restrictItems.add("Cremă de cofetărie gata preparată");
        restrictItems.add("Dulciuri");
        restrictItems.add("Salamuri");
        restrictItems.add("Bastoane de crab și toate produsele semifabricate din magazinul de carne");
        restrictItems.add("Toate alimentele cu urme de gluten în compoziția lor sunt interzise (de exemplu, amidon de grâu)");
    }

    private void setupRecyclerView() {
        recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new RestrictRecyclerViewAdapter(getContext(), restrictItems);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position) {
    }
}