package com.example.sorinateza;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.sorinateza.models.Produs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ayalma.ir.expandablerecyclerview.ExpandableRecyclerView;

import static android.graphics.Typeface.*;
import static android.graphics.Typeface.DEFAULT_BOLD;

public class CalculatorAdapter extends ExpandableRecyclerView.Adapter<
        CalculatorAdapter.ViewHolder,
        CalculatorAdapter.ViewHolderHeader,
        Produs,
        String> {

    private final Map<String, List<Produs>> mData;
    private final LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    CalculatorAdapter(Context context, Map<String, List<Produs>> data) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;


        setOnChildItemClickedListener(new ExpandableRecyclerView.OnChildItemClickedListener() {
            @Override
            public void onChildItemClicked(int group, int position) {

            }
        });
    }

//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = mInflater.inflate(R.layout.calculator_row, parent, false);
//        return new ViewHolder(view);
//    }

    @Override
    protected ViewHolderHeader onCreateGroupViewHolder(ViewGroup parent) {
        View view = mInflater.inflate(R.layout.calculator_row_header, parent, false);
        return new ViewHolderHeader(view);
    }

    @Override
    protected ViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
//        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_list_content,parent,false);
//        return new ChildViewHolder(rootView);

        View view = mInflater.inflate(R.layout.calculator_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getChildItemViewType(int group, int position) {
        return 1;
    }
//
//    @Override
//    public void onBindChildViewHolder(ViewHolder holder, int group, int position) {
//        super.onBindChildViewHolder(holder, group, position);
//
//        List<String> stringsList = new ArrayList<>(mData.keySet());
//        List<Produs> produsList = mData.get(stringsList.get(group));
//
//        Produs produs = produsList.get(position);
//        holder.updateView(produs);
//    }

//    @Override
//    public void onBindGroupViewHolder(ViewHolderHeader holder, int group) {
//        super.onBindGroupViewHolder(holder, group);
//
//        holder.updateView("sfdsadsadas");
//    }

    //
//    @Override
//    public void onBindGroupViewHolder(ExpandableRecyclerView.SimpleGroupViewHolder holder, int group) {
//        super.onBindGroupViewHolder(holder, group);
//
//        holder.setText("sdfsdfsdfsd");
//    }

    @Override
    public int getGroupItemCount() {
        return mData.size();
    }

    @Override
    public int getChildItemCount(int group) {
        List<String> stringsList = new ArrayList<>(mData.keySet());
        List<Produs> produsList = mData.get(stringsList.get(group));
        return produsList.size();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public String getGroupItem(int position) {
        List<String> stringsList = new ArrayList<>(mData.keySet());
        return stringsList.get(position);
    }

    @Override
    public Produs getChildItem(int group, int position) {
        List<String> stringsList = new ArrayList<>(mData.keySet());
        List<Produs> produsList = mData.get(stringsList.get(group));

        return produsList.get(position);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(int position);
    }

    public List<Produs> getSelectedProduse() {
        List<Produs> selectedmata = new ArrayList<>();

        for (Map.Entry<String, List<Produs>> entry : mData.entrySet()) {
            for (Produs data:
                    entry.getValue()) {
                if (data.isChecked()) {
                    selectedmata.add(data);
                }
            }
        }

        return selectedmata;
    }

    public boolean hasSelected() {
        for (Map.Entry<String, List<Produs>> entry : mData.entrySet()) {
            for (Produs produs :
                    entry.getValue()) {
                if (produs.isChecked()) {
                    return true;
                }
            }
        }

        return false;
    }

    public class ViewHolder extends ExpandableRecyclerView.GroupViewHolder implements View.OnClickListener {
        TextView titleTextView;
        RadioButton radioButton;
        TextView proteineText;
        TextView lipideText;
        TextView glucideText;
        LinearLayout proteineLayout;

        ViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.title_text_view);
            radioButton = itemView.findViewById(R.id.radio);
            proteineText = itemView.findViewById(R.id.proteine_text);
            lipideText = itemView.findViewById(R.id.lipide_text);
            glucideText = itemView.findViewById(R.id.glucide_text);
            proteineLayout = itemView.findViewById(R.id.proteine_layout);

            itemView.setOnClickListener(this);
        }

        @Override
        public void expand() {

        }

        @Override
        public void collapse() {

        }

        @Override
        public void setExpanded(boolean expanded) {

        }

        @Override
        public boolean isExpanded() {
            return false;
        }

        public void updateView(Produs produs) {
            titleTextView.setText(produs.getProdusul());
            radioButton.setChecked(produs.isChecked());
            proteineText.setText(limitString(produs.getProteine()));
            lipideText.setText(limitString(produs.getLipide()));
            glucideText.setText(limitString(produs.getGlucide()));

            proteineLayout.setVisibility(produs.isSection() ? View.GONE : View.VISIBLE);
            radioButton.setVisibility(produs.isSection() ? View.GONE : View.VISIBLE);

            if (produs.isSection()) titleTextView.setTypeface(DEFAULT_BOLD);
            else titleTextView.setTypeface(Typeface.defaultFromStyle(NORMAL));

        }

        @Override
        public void onClick(View view) {
//            radioButton.setChecked(!radioButton.isChecked());
//
//            getItem(getAdapterPosition()).setChecked(radioButton.isChecked());
//
//            if (mClickListener != null) mClickListener.onItemClick(getAdapterPosition());
//
//            if (radioButton.isChecked()) {
//                showInputDialog(getItem(getAdapterPosition()));
//            }
        }

        private void showInputDialog(Produs produs) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Introduceti cantitatea de produs (g):");

            final EditText input = new EditText(context);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            builder.setView(input);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (input.getText().toString().isEmpty() || input.getText() == null || input.getText().toString() == null) {
                        produs.setCantitateGrame(100);
                    } else {
                        produs.setCantitateGrame(Integer.parseInt(input.getText().toString()));
                    }

                    if (mClickListener != null) mClickListener.onItemClick(getAdapterPosition());
                }
            });

            builder.setNegativeButton("Inchide", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    radioButton.setChecked(false);
//                    getItem(getAdapterPosition()).setChecked(radioButton.isChecked());

                    if (mClickListener != null) mClickListener.onItemClick(getAdapterPosition());
                    dialog.cancel();
                }
            });

            builder.show();
        }

        private String limitString(String text) {
            if (text.length() > 3) {
                String cutString = text.substring(0, 3);
                return cutString;
            } else {
                return text;
            }
        }
    }

    public class ViewHolderHeader extends ExpandableRecyclerView.GroupViewHolder {
        TextView titleTextView;

        ViewHolderHeader(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.text_header);
        }

        @Override
        public void expand() {

        }

        @Override
        public void collapse() {

        }

        @Override
        public void setExpanded(boolean expanded) {

        }

        @Override
        public boolean isExpanded() {
            return false;
        }

        public void updateView(String text) {
            titleTextView.setText(text);
        }
    }

}