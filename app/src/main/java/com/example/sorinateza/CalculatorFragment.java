package com.example.sorinateza;

import android.os.Bundle;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sorinateza.models.Produs;
import com.example.sorinateza.models.ProdusData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ayalma.ir.expandablerecyclerview.ExpandableRecyclerView;


/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class CalculatorFragment extends Fragment implements CalculatorAdapter.ItemClickListener {
    private View rootView;
    private ExpandableRecyclerView recyclerView;
    private Button nextButton;
    private Map<String, List<Produs>> calculatorItems = new ArrayMap<>();
    private CalculatorAdapter adapter;

    public CalculatorFragment() {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CalculatorFragment newInstance() {
        return new CalculatorFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.calculator_layout, container, false);
        setupRestrictList();
        setupRecyclerView();
        setupNextButton();
        nextButton.setEnabled(adapter.hasSelected());

        getActivity().setTitle("Calculator calorii");
        return rootView;
    }

    private void setupNextButton() {
        nextButton.setOnClickListener(v -> {
            CalculatorDetailFragment.calculatorItems = adapter.getSelectedProduse();
            openFragment(CalculatorDetailFragment.newInstance());
        });
    }

    private void setupRestrictList() {
        calculatorItems = ProdusData.getListMapProduse(getContext());
    }

    private void setupRecyclerView() {
        recyclerView = rootView.findViewById(R.id.recyclerView);
        nextButton = rootView.findViewById(R.id.next_button);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new CalculatorAdapter(getContext(), calculatorItems);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    private void openFragment(Fragment fragment) {
        if (getActivity() != null) {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    @Override
    public void onItemClick(int position) {
        nextButton.setEnabled(adapter.hasSelected());
    }
}