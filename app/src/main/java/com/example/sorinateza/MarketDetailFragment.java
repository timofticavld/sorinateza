package com.example.sorinateza;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sorinateza.models.Market;

public class MarketDetailFragment extends Fragment {

    public static String MARKET_KEY = "MARKET_KEY";
    private Market market;
    private View root;
    private TextView denumireProdus, producator, cantitate, glucide, proteine, grasimi, valoareEnergetica, fibre, vitamine, elementeMinerale, denumireMarket, pret;
    private LinearLayout back;

    public static MarketDetailFragment newInstance(Market market) {
        MarketDetailFragment fragment = new MarketDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(MARKET_KEY, market);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            market = getArguments().getParcelable(MARKET_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_market_detail, container, false);
        initView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupData();
        back.setOnClickListener(click -> {
            if (getActivity() != null) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    private void setupData() {
        denumireProdus.setText(market.getDenumireprodus());
        producator.setText(market.getProducător());
        cantitate.setText(market.getCantitate());
        glucide.setText(market.getGLUCIDE());
        proteine.setText(market.getPROTEINE());
        grasimi.setText(market.getGRĂSIMI());
        valoareEnergetica.setText(market.getValoareaenergetică());
        fibre.setText(market.getFibre());
        vitamine.setText(market.getVitamine());
        elementeMinerale.setText(market.getElementeminerale());
        denumireMarket.setText(market.getDenumiremarket());
        pret.setText(market.getPreț() + " Lei");
    }

    private void initView() {
        denumireProdus = root.findViewById(R.id.productName);
        producator = root.findViewById(R.id.producatorName);
        cantitate = root.findViewById(R.id.cantitateTitle);
        glucide = root.findViewById(R.id.glucideTitle);
        proteine = root.findViewById(R.id.proteineTitle);
        grasimi = root.findViewById(R.id.grasimiTitle);
        valoareEnergetica = root.findViewById(R.id.valoareEnerTitle);
        fibre = root.findViewById(R.id.fibreTitle);
        vitamine = root.findViewById(R.id.vitamineTitle);
        elementeMinerale = root.findViewById(R.id.elementeMineTitle);
        denumireMarket = root.findViewById(R.id.marketTitle);
        pret = root.findViewById(R.id.pretTitle);
        back = root.findViewById(R.id.back);

    }

}