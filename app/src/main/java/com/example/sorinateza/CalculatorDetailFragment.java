package com.example.sorinateza;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.sorinateza.models.Produs;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class CalculatorDetailFragment extends Fragment implements OnChartGestureListener {
    public static final String APP_PREFERENCES = "mysettings";
    public static List<Produs> calculatorItems = new ArrayList<>();
    private final String[] mLabels = new String[]{"Consum calorii zilnic:"};
    DecoView decoView;
    float kcal = 0;
    private View rootView;
    private Button saveButton;
    private TextView textView;

    public CalculatorDetailFragment() {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CalculatorDetailFragment newInstance() {
        return new CalculatorDetailFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.calculator_detail_fragment, container, false);
        setupRecyclerView();
        setupSaveButton();
        setupTextView();
        setupBarChart();

        getActivity().setTitle("Calculator calorii");
        return rootView;
    }

    private void setupBarChart() {
        int max = (int) SettingsFragment.calculNecesarulEnergetic(getContext());

        SeriesItem seriesItem = new SeriesItem.Builder(Color.parseColor("#FFE2E2E2"))
                .setRange(0, max, 0)
                .build();

        int backIndex = decoView.addSeries(seriesItem);

        final SeriesItem series1Item = new SeriesItem.Builder(Color.parseColor("#FFFF8800"))
                .setRange(0, max, 0)
                .build();

        int series1Index = decoView.addSeries(series1Item);

        final TextView textPercentage = (TextView) rootView.findViewById(R.id.textPercentage);
        series1Item.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilled = ((currentPosition - series1Item.getMinValue()) / (series1Item.getMaxValue() - series1Item.getMinValue()));
                textPercentage.setText(String.format("%.0f%%", percentFilled * 100));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        decoView.addEvent(new DecoEvent.Builder(max)
                .setIndex(backIndex)
                .setDuration(0010)
                .build());

        int perc = (int) kcal;
        decoView.addEvent(new DecoEvent.Builder(perc)
                .setIndex(series1Index)
                .setDuration(1000)
                .build());
    }

    private void saveDayData() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        int call = sharedpreferences.getInt(((""+mYear) + (mMonth + "") + ("" + mDay)) + "_day", 0);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(((""+mYear) + (mMonth + "") + ("" + mDay)) + "_day", call + (int)kcal);
        editor.apply();
        getActivity().onBackPressed();
    }

    public double calculatePercentage(double obtained, double total) {
        return obtained * 100 / total;
    }

    private void setupSaveButton() {
        saveButton.setOnClickListener(v -> {
            saveDayData();
        });
    }

    private void setupTextView() {
        StringBuilder text = new StringBuilder();

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        int call = sharedpreferences.getInt(((""+mYear) + (mMonth + "") + ("" + mDay)) + "_day", 0);
        float kcal = call;

        text.append("\n");
        for (Produs produs :
                calculatorItems) {
            float cal = 0;
            cal = (Float.valueOf(produs.getGlucide()) * 4) +
                    (Float.valueOf(produs.getLipide()) * 9) +
                    (Float.valueOf(produs.getProteine()) * 4);
            cal = (cal * produs.getCantitateGrame()) / 100;

            String produsInfo = produs.getProdusul() + ":  " + produs.getCantitateGrame() + "g\n";
            text.append(produsInfo);

            kcal = kcal + cal;
        }
        text.append("\nCalorii consumate total (kcal):  " + kcal + "\n");
        this.kcal = kcal;

        textView.setText(text);
    }

    private void setupRecyclerView() {
        saveButton = rootView.findViewById(R.id.saveButton);
        textView = rootView.findViewById(R.id.textView);
        decoView = (DecoView) rootView.findViewById(R.id.dynamicArcView);
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }
}