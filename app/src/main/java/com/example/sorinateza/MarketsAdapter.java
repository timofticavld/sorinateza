package com.example.sorinateza;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sorinateza.models.Market;

import java.util.List;

public class MarketsAdapter extends RecyclerView.Adapter<MarketsAdapter.ViewHolder> {
    List<Market> markets;
    private ItemClickListener mClickListener;
    private final LayoutInflater mInflater;

    MarketsAdapter(Context context, List<Market> data, ItemClickListener clickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.markets = data;
        this.mClickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.market_item, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MarketsAdapter.ViewHolder holder, int position) {
        Market market = getItem(position);
        holder.marketName.setText(market.getDenumiremarket());
        holder.producatorName.setText(market.getProducător());
        holder.productName.setText(market.getDenumireprodus());
        holder.price.setText(market.getPreț() + " Lei");
        holder.itemView.setOnClickListener(click -> mClickListener.onItemClick(market));
    }

    @Override
    public int getItemCount() {
        return markets.size();
    }

    Market getItem(int id) {
        return markets.get(id);
    }

    public interface ItemClickListener {
        void onItemClick(Market market);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView marketName, producatorName, price, productName;

        ViewHolder(View itemView) {
            super(itemView);
            marketName = itemView.findViewById(R.id.marketName);
            producatorName = itemView.findViewById(R.id.producatorName);
            price = itemView.findViewById(R.id.price);
            productName = itemView.findViewById(R.id.productName);
        }
    }


}
