package com.example.sorinateza;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.sorinateza.models.MarketUtility;
import com.example.sorinateza.models.ProdusData;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Calendar;

import static com.example.sorinateza.models.MarketUtility.getListMapMarkets;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigation;
    private int mYear, mMonth, mDay;
    public static final String APP_PREFERENCES = "mysettings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        bottomNavigation = findViewById(R.id.bottom_navigation);
        BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
                item -> {
                    switch (item.getItemId()) {
                        case R.id.navigation_info:
                            openFragment(InfoFragment.newInstance());
                            return true;
                        case R.id.navigation_restrict:
                            openFragment(RestrictFragment.newInstance());
                            return true;
                        case R.id.navigation_calculator:
                            openFragment(CalculatorFragment.newInstance());
                            return true;
                        case R.id.navigation_places:
                            openFragment(SettingsFragment.newInstance());
                            return true;
                        case R.id.navigation_markets:
                            openFragment(MarketsFragment.newInstance());
                            return true;
                    }
                    return false;
                };

        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        openFragment(InfoFragment.newInstance());

        showDailyActivityDialog();
        getAge();
        showGenderDialog();

        getListMapMarkets(this);
    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void datePickerDialog() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    savePreferences(getAge(year, monthOfYear, dayOfMonth));
                }, mYear, mMonth, mDay);

        datePickerDialog.setTitle("Va rugam sa indicati anul nasterii!");
        datePickerDialog.show();
    }

    private int getAge(int year, int month, int day){
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        return age;
    }

    private void savePreferences(int age) {
        SharedPreferences sharedpreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("age", age);
        editor.apply();
    }

    private void saveGenderPreferences(int age) {
        SharedPreferences sharedpreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("sex", age);
        editor.apply();
    }

    private void getAge() {
        SharedPreferences sharedpreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);

        if (sharedpreferences.getInt("age", 0) == 0) {
            datePickerDialog();
        } else {
            System.out.println("age == " + " "  + sharedpreferences.getInt("age", 0));
        }
    }

    private void showGenderDialog() {
        SharedPreferences sharedpreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);

        if (sharedpreferences.getInt("sex", 99) != 99) {
            return;
        }

        String[] singleChoiceItems = getResources().getStringArray(R.array.dialog_single_choice_array);
        int itemSelected = 0;
        new AlertDialog.Builder(this)
                .setTitle("Indicati sexul:")
                .setSingleChoiceItems(singleChoiceItems, itemSelected, (dialogInterface, selectedIndex) -> {
                    saveGenderPreferences(itemSelected);
                })
                .setPositiveButton("Ok", null)
                .setNegativeButton("Inchide", null)
                .show();
    }

    private void showDailyActivityDialog() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        SharedPreferences sharedpreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);

        if (sharedpreferences.getInt(String.valueOf(mYear+mMonth+mDay), 99) != 99) {
            return;
        }

        String[] singleChoiceItems = getResources().getStringArray(R.array.dialog_activitate_choice_array);
        int itemSelected = 0;
        new AlertDialog.Builder(this)
                .setTitle("Indicati nivelul de activitate zilnica:")
                .setSingleChoiceItems(singleChoiceItems, itemSelected, (dialogInterface, selectedIndex) -> {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt(String.valueOf(mYear+mMonth+mDay), selectedIndex);
                    editor.apply();
                })
                .setPositiveButton("Ok", null)
                .setNegativeButton("Inchide", null)
                .show();
    }

}