package com.example.sorinateza;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class InfoFragment extends Fragment {

    private String text = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n" +
            "<HTML>\n" +
            "<HEAD>\n" +
            "\t<META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=utf-8\">\n" +
            "\t<TITLE></TITLE>\n" +
            "\t<META NAME=\"GENERATOR\" CONTENT=\"LibreOffice 4.1.6.2 (Linux)\">\n" +
            "\t<META NAME=\"AUTHOR\" CONTENT=\"Sorina Doboş\">\n" +
            "\t<META NAME=\"CREATED\" CONTENT=\"20210426;210400000000000\">\n" +
            "\t<META NAME=\"CHANGEDBY\" CONTENT=\"Sorina Doboş\">\n" +
            "\t<META NAME=\"CHANGED\" CONTENT=\"20210426;213500000000000\">\n" +
            "\t<META NAME=\"AppVersion\" CONTENT=\"16.0000\">\n" +
            "\t<META NAME=\"DocSecurity\" CONTENT=\"0\">\n" +
            "\t<META NAME=\"HyperlinksChanged\" CONTENT=\"false\">\n" +
            "\t<META NAME=\"LinksUpToDate\" CONTENT=\"false\">\n" +
            "\t<META NAME=\"ScaleCrop\" CONTENT=\"false\">\n" +
            "\t<META NAME=\"ShareDoc\" CONTENT=\"false\">\n" +
            "\t<STYLE TYPE=\"text/css\">\n" +
            "\t<!--\n" +
            "\t\t@page { margin-left: 1.18in; margin-right: 0.59in; margin-top: 0.79in; margin-bottom: 0.79in }\n" +
            "\t\tP { margin-bottom: 0.08in; direction: ltr; widows: 2; orphans: 2 }\n" +
            "\t-->\n" +
            "\t</STYLE>\n" +
            "</HEAD>\n" +
            "<BODY LANG=\"ru-RU\" DIR=\"LTR\">\n" +
            "<P LANG=\"ro-MD\" ALIGN=CENTER STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3><FONT SIZE=4><B>Dieta\n" +
            "fără gluten</B></FONT></FONT></FONT></P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>Mulți cred că\n" +
            "dieta fără gluten este pur și simplu o modalitate rapidă de a\n" +
            "slăbi. Totuși, acest lucru nu este adevărat. Dieta fără gluten\n" +
            "este în prezent singurul tratament pentru persoanele cu boală\n" +
            "celiacă. Persoanele care trăiesc cu sensibilitate la gluten\n" +
            "non-celiac beneficiază, de asemenea, de consumul fără gluten.\n" +
            "Deoarece nu există pastile sau terapii disponibile, singura\n" +
            "modalitate de a trata boala celiacă este printr-o dietă strictă,\n" +
            "100% fără gluten. </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=CENTER STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3><FONT SIZE=4><B>Deci,\n" +
            "ce este totuși glutenul ?</B></FONT></FONT></FONT></P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>Glutenul este o\n" +
            "protein<FONT FACE=\"Calibri, serif\">ă</FONT> care se g<FONT FACE=\"Calibri, serif\">ă</FONT>se<FONT FACE=\"Calibri, serif\">ș</FONT>te\n" +
            "în grâu, orz, secar<FONT FACE=\"Calibri, serif\">ă</FONT> <FONT FACE=\"Calibri, serif\">ș</FONT>i\n" +
            "deriva<FONT FACE=\"Calibri, serif\">ț</FONT>ii acestor boabe, inclusiv\n" +
            "mal<FONT FACE=\"Calibri, serif\">ț</FONT>ul <FONT FACE=\"Calibri, serif\">ș</FONT>i\n" +
            "drojdia de bere. O diet<FONT FACE=\"Calibri, serif\">ă</FONT> f<FONT FACE=\"Calibri, serif\">ă</FONT>r<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "gluten exclude toate produsele care con<FONT FACE=\"Calibri, serif\">ț</FONT>in\n" +
            "aceste ingrediente. Cei care nu con<FONT FACE=\"Calibri, serif\">ț</FONT>in\n" +
            "gluten se pot bucura în continuare de o diet<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "s<FONT FACE=\"Calibri, serif\">ă</FONT>n<FONT FACE=\"Calibri, serif\">ă</FONT>toas<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "plin<FONT FACE=\"Calibri, serif\">ă</FONT> de fructe, legume, carne,\n" +
            "pe<FONT FACE=\"Calibri, serif\">ș</FONT>te, leguminoase <FONT FACE=\"Calibri, serif\">ș</FONT>i\n" +
            "majoritatea produselor lactate. Astfel de ingrediente nu con<FONT FACE=\"Calibri, serif\">ț</FONT>in\n" +
            "gluten în mod natural <FONT FACE=\"Calibri, serif\">ș</FONT>i sunt\n" +
            "sigure pentru persoanele care nu au alergii la aceste grupuri de\n" +
            "alimente respective.</FONT></FONT></P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=CENTER STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3><FONT SIZE=4><B>Alimente\n" +
            "interzise</B></FONT></FONT></FONT></P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- grâu, secar<FONT FACE=\"Calibri, serif\">ă</FONT>,\n" +
            "orz, ov<FONT FACE=\"Calibri, serif\">ă</FONT>z <FONT FACE=\"Calibri, serif\">ș</FONT>i\n" +
            "to<FONT FACE=\"Calibri, serif\">ț</FONT>i deriva<FONT FACE=\"Calibri, serif\">ț</FONT>ii\n" +
            "acestora; </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- orz perlat, griș,\n" +
            "cuscus; </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- pâine obi<FONT FACE=\"Calibri, serif\">ș</FONT>nuit<FONT FACE=\"Calibri, serif\">ă</FONT>,\n" +
            "pesmeți <FONT FACE=\"Calibri, serif\">ș</FONT>i paste; </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- conserve\n" +
            "industriale; </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- mese gata\n" +
            "preparate (supe, sosuri, maioneza); </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- cofet<FONT FACE=\"Calibri, serif\">ă</FONT>rie,\n" +
            "budinci, crem<FONT FACE=\"Calibri, serif\">ă</FONT> de cofet<FONT FACE=\"Calibri, serif\">ă</FONT>rie\n" +
            "gata preparat<FONT FACE=\"Calibri, serif\">ă</FONT>, dulciuri; </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- salamuri, bastoane\n" +
            "de crab <FONT FACE=\"Calibri, serif\">ș</FONT>i toate produsele\n" +
            "semifabricate din magazinul de carne; </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- Toate alimentele\n" +
            "cu urme de gluten în compozi<FONT FACE=\"Calibri, serif\">ț</FONT>ia\n" +
            "lor sunt interzise (de exemplu, amidon de grâu).</FONT></FONT></P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=CENTER STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3><FONT SIZE=4><B>Pro\n" +
            "</B></FONT><FONT FACE=\"Calibri, serif\"><FONT SIZE=4><B>ș</B></FONT></FONT><FONT SIZE=4><B>i\n" +
            "contra ale unei diete f</B></FONT><FONT FACE=\"Calibri, serif\"><FONT SIZE=4><B>ă</B></FONT></FONT><FONT SIZE=4><B>r</B></FONT><FONT FACE=\"Calibri, serif\"><FONT SIZE=4><B>ă</B></FONT></FONT><FONT SIZE=4><B>\n" +
            "gluten</B></FONT></FONT></FONT></P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3><I><B>1. Avantaje\n" +
            "ale unei diete f</B></I><FONT FACE=\"Calibri, serif\"><I><B>ă</B></I></FONT><I><B>r</B></I><FONT FACE=\"Calibri, serif\"><I><B>ă</B></I></FONT><I><B>\n" +
            "gluten: </B></I></FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- Mâncare simpl<FONT FACE=\"Calibri, serif\">ă</FONT>,\n" +
            "s<FONT FACE=\"Calibri, serif\">ă</FONT>n<FONT FACE=\"Calibri, serif\">ă</FONT>toas<FONT FACE=\"Calibri, serif\">ă</FONT>.\n" +
            "</FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- U<FONT FACE=\"Calibri, serif\">ș</FONT>or\n" +
            "de <FONT FACE=\"Calibri, serif\">ț</FONT>inut. </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- O astfel de diet<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "ajut<FONT FACE=\"Calibri, serif\">ă</FONT> la reducerea manifest<FONT FACE=\"Calibri, serif\">ă</FONT>rii\n" +
            "reac<FONT FACE=\"Calibri, serif\">ț</FONT>iilor alergice ale pielii,\n" +
            "prin urmare, aceast<FONT FACE=\"Calibri, serif\">ă</FONT> diet<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "este adesea prescris<FONT FACE=\"Calibri, serif\">ă</FONT> de\n" +
            "dermatologi persoanelor care sufer<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "de dermatit<FONT FACE=\"Calibri, serif\">ă</FONT> alergic<FONT FACE=\"Calibri, serif\">ă</FONT>.\n" +
            "</FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- Dieta ajut<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "nu numai la cur<FONT FACE=\"Calibri, serif\">ăț</FONT>area corpului,\n" +
            "ci <FONT FACE=\"Calibri, serif\">ș</FONT>i la pierderea excesului de\n" +
            "greutate. Promoveaz<FONT FACE=\"Calibri, serif\">ă</FONT> eliminarea\n" +
            "toxinelor <FONT FACE=\"Calibri, serif\">ș</FONT>i a toxinelor care\n" +
            "s-au acumulat în organism pe o perioad<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "lung<FONT FACE=\"Calibri, serif\">ă</FONT> de timp. </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- Foarte util pentru\n" +
            "copiii mici (cu digestie neformat<FONT FACE=\"Calibri, serif\">ă</FONT>,\n" +
            "glutenul determin<FONT FACE=\"Calibri, serif\">ă</FONT> formarea de\n" +
            "colici <FONT FACE=\"Calibri, serif\">ș</FONT>i gaze). </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- Dieta este variată\n" +
            "și echilibrată, ceea ce exclude epuizarea corpului. </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- Urmând o dietă\n" +
            "fără gluten, nu veți experimenta senzațiile acute de foame, așa\n" +
            "cum este posibil cu alte regimuri dietetice.</FONT></FONT></P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3><I><B>2. Contra unei\n" +
            "diete f</B></I><FONT FACE=\"Calibri, serif\"><I><B>ă</B></I></FONT><I><B>r</B></I><FONT FACE=\"Calibri, serif\"><I><B>ă</B></I></FONT><I><B>\n" +
            "gluten: </B></I></FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- Lipsa posibil<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "de vitamine (în special grupul B <FONT FACE=\"Calibri, serif\">ș</FONT>i\n" +
            "minerale. </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- Dificil de urmat\n" +
            "pentru iubitorii de paste, produse de panificație și cofetărie. </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>- Comparativ cu alte\n" +
            "diete restrictive din anumite alimente, f<FONT FACE=\"Calibri, serif\">ă</FONT>r<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "gluten este mult mai pu<FONT FACE=\"Calibri, serif\">ț</FONT>in\n" +
            "periculos datorit<FONT FACE=\"Calibri, serif\">ă</FONT> listei largi\n" +
            "de alimente disponibile. În acest sens, probabilitatea unei\n" +
            "defec<FONT FACE=\"Calibri, serif\">ț</FONT>iuni este minim<FONT FACE=\"Calibri, serif\">ă</FONT>,\n" +
            "la fel <FONT FACE=\"Calibri, serif\">ș</FONT>i pericolul lipsei de\n" +
            "nutrien<FONT FACE=\"Calibri, serif\">ț</FONT>i <FONT FACE=\"Calibri, serif\">ș</FONT>i\n" +
            "vitamine. </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=CENTER STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3><FONT SIZE=4><B>Cine\n" +
            "ar trebui să urmeze o dietă fără gluten</B></FONT></FONT></FONT></P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>Cu siguran<FONT FACE=\"Calibri, serif\">ță</FONT>,\n" +
            "persoanele care sufer<FONT FACE=\"Calibri, serif\">ă</FONT> de o boal<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "destul de rar<FONT FACE=\"Calibri, serif\">ă</FONT> - boala celiac<FONT FACE=\"Calibri, serif\">ă</FONT>,\n" +
            "ar trebui s<FONT FACE=\"Calibri, serif\">ă</FONT> adere cu siguran<FONT FACE=\"Calibri, serif\">ță</FONT>\n" +
            "la o diet<FONT FACE=\"Calibri, serif\">ă</FONT> f<FONT FACE=\"Calibri, serif\">ă</FONT>r<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "gluten. De asemenea, medicii recomand<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "acest sistem nutri<FONT FACE=\"Calibri, serif\">ț</FONT>ional pentru\n" +
            "hipersensibilitate sau alergie la gluten, precum <FONT FACE=\"Calibri, serif\">ș</FONT>i\n" +
            "pentru autism. O persoan<FONT FACE=\"Calibri, serif\">ă</FONT> modern<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "sufer<FONT FACE=\"Calibri, serif\">ă</FONT> din ce în ce mai mult de\n" +
            "reac<FONT FACE=\"Calibri, serif\">ț</FONT>ii alergice <FONT FACE=\"Calibri, serif\">ș</FONT>i,\n" +
            "prin urmare, o respingere complet<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "sau par<FONT FACE=\"Calibri, serif\">ț</FONT>ial<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "a glutenului poate îmbun<FONT FACE=\"Calibri, serif\">ă</FONT>t<FONT FACE=\"Calibri, serif\">ăț</FONT>i\n" +
            "s<FONT FACE=\"Calibri, serif\">ă</FONT>n<FONT FACE=\"Calibri, serif\">ă</FONT>tatea\n" +
            "<FONT FACE=\"Calibri, serif\">ș</FONT>i poate sc<FONT FACE=\"Calibri, serif\">ă</FONT>pa\n" +
            "de alergii. </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>Oamenii s<FONT FACE=\"Calibri, serif\">ă</FONT>n<FONT FACE=\"Calibri, serif\">ă</FONT>to<FONT FACE=\"Calibri, serif\">ș</FONT>i\n" +
            "nu au niciun sens s<FONT FACE=\"Calibri, serif\">ă</FONT> mearg<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "la aceast<FONT FACE=\"Calibri, serif\">ă</FONT> diet<FONT FACE=\"Calibri, serif\">ă</FONT>.\n" +
            "Acest lucru este prea dificil, deoarece glutenul se g<FONT FACE=\"Calibri, serif\">ă</FONT>se<FONT FACE=\"Calibri, serif\">ș</FONT>te\n" +
            "adesea în multe produse, este adesea folosit ca agent de îngro<FONT FACE=\"Calibri, serif\">ș</FONT>are\n" +
            "<FONT FACE=\"Calibri, serif\">ș</FONT>i este foarte dificil s<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "se abandoneze produsele din cereale în lumea modern<FONT FACE=\"Calibri, serif\">ă</FONT>.\n" +
            "O diet<FONT FACE=\"Calibri, serif\">ă</FONT> f<FONT FACE=\"Calibri, serif\">ă</FONT>r<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "gluten este necesar<FONT FACE=\"Calibri, serif\">ă</FONT> fie pentru\n" +
            "pierderea în greutate, fie din motive medicale. </FONT></FONT>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=CENTER STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3><FONT SIZE=4><B>Cum\n" +
            "arat</B></FONT><FONT FACE=\"Calibri, serif\"><FONT SIZE=4><B>ă</B></FONT></FONT><FONT SIZE=4><B>\n" +
            "o diet</B></FONT><FONT FACE=\"Calibri, serif\"><FONT SIZE=4><B>ă</B></FONT></FONT><FONT SIZE=4><B>\n" +
            "s</B></FONT><FONT FACE=\"Calibri, serif\"><FONT SIZE=4><B>ă</B></FONT></FONT><FONT SIZE=4><B>n</B></FONT><FONT FACE=\"Calibri, serif\"><FONT SIZE=4><B>ă</B></FONT></FONT><FONT SIZE=4><B>toas</B></FONT><FONT FACE=\"Calibri, serif\"><FONT SIZE=4><B>ă</B></FONT></FONT><FONT SIZE=4><B>\n" +
            "f</B></FONT><FONT FACE=\"Calibri, serif\"><FONT SIZE=4><B>ă</B></FONT></FONT><FONT SIZE=4><B>r</B></FONT><FONT FACE=\"Calibri, serif\"><FONT SIZE=4><B>ă</B></FONT></FONT><FONT SIZE=4><B>\n" +
            "gluten?</B></FONT></FONT></FONT></P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\"><A NAME=\"_GoBack\"></A>\n" +
            "<FONT FACE=\"Times New Roman, serif\"><FONT SIZE=3>În mod\n" +
            "surprinz<FONT FACE=\"Calibri, serif\">ă</FONT>tor, este similar cu o\n" +
            "diet<FONT FACE=\"Calibri, serif\">ă</FONT> s<FONT FACE=\"Calibri, serif\">ă</FONT>n<FONT FACE=\"Calibri, serif\">ă</FONT>toas<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "în mod tradi<FONT FACE=\"Calibri, serif\">ț</FONT>ional - sunt\n" +
            "necesare pu<FONT FACE=\"Calibri, serif\">ț</FONT>ine alimente de lux.\n" +
            "Umple<FONT FACE=\"Calibri, serif\">ț</FONT>i farfuria cu alimente\n" +
            "s<FONT FACE=\"Calibri, serif\">ă</FONT>n<FONT FACE=\"Calibri, serif\">ă</FONT>toase\n" +
            "f<FONT FACE=\"Calibri, serif\">ă</FONT>r<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "gluten, cum ar fi legume, fructe, fasole, nuci, semin<FONT FACE=\"Calibri, serif\">ț</FONT>e,\n" +
            "pe<FONT FACE=\"Calibri, serif\">ș</FONT>te <FONT FACE=\"Calibri, serif\">ș</FONT>i\n" +
            "carne slab<FONT FACE=\"Calibri, serif\">ă</FONT>. Aceasta este ceea ce\n" +
            "dieteticienii recomand<FONT FACE=\"Calibri, serif\">ă</FONT> pentru a\n" +
            "face majoritatea dietei, indiferent dac<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "sunte<FONT FACE=\"Calibri, serif\">ț</FONT>i f<FONT FACE=\"Calibri, serif\">ă</FONT>r<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "gluten sau nu. <FONT FACE=\"Calibri, serif\">Ș</FONT>i dac<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "iube<FONT FACE=\"Calibri, serif\">ș</FONT>ti boabele, le po<FONT FACE=\"Calibri, serif\">ț</FONT>i\n" +
            "mânca în continuare. Atât de mul<FONT FACE=\"Calibri, serif\">ț</FONT>i\n" +
            "oameni cred c<FONT FACE=\"Calibri, serif\">ă</FONT> f<FONT FACE=\"Calibri, serif\">ă</FONT>r<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "gluten nu înseamn<FONT FACE=\"Calibri, serif\">ă</FONT> deloc boabe,\n" +
            "dar exist<FONT FACE=\"Calibri, serif\">ă</FONT> atât de multe op<FONT FACE=\"Calibri, serif\">ț</FONT>iuni\n" +
            "excelente f<FONT FACE=\"Calibri, serif\">ă</FONT>r<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "gluten cum ar fi orezul, meiul, quinoa <FONT FACE=\"Calibri, serif\">ș</FONT>i\n" +
            "hri<FONT FACE=\"Calibri, serif\">ș</FONT>ca. Ast<FONT FACE=\"Calibri, serif\">ă</FONT>zi,\n" +
            "pute<FONT FACE=\"Calibri, serif\">ț</FONT>i g<FONT FACE=\"Calibri, serif\">ă</FONT>si\n" +
            "cu u<FONT FACE=\"Calibri, serif\">ș</FONT>urin<FONT FACE=\"Calibri, serif\">ță</FONT>\n" +
            "paste f<FONT FACE=\"Calibri, serif\">ă</FONT>r<FONT FACE=\"Calibri, serif\">ă</FONT>\n" +
            "gluten f<FONT FACE=\"Calibri, serif\">ă</FONT>cute din porumb, quinoa\n" +
            "sau fasole. În mod obi<FONT FACE=\"Calibri, serif\">ș</FONT>nuit,\n" +
            "bolnavii de celiaci nou diagnostica<FONT FACE=\"Calibri, serif\">ț</FONT>i\n" +
            "sunt deficien<FONT FACE=\"Calibri, serif\">ț</FONT>i în fibre, fier,\n" +
            "calciu, vitamina D <FONT FACE=\"Calibri, serif\">ș</FONT>i chiar\n" +
            "proteine, potrivit funda<FONT FACE=\"Calibri, serif\">ț</FONT>iei\n" +
            "pentru boala celiac<FONT FACE=\"Calibri, serif\">ă</FONT>.</FONT></FONT></P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<BR><BR>\n" +
            "</P>\n" +
            "<P LANG=\"ro-MD\" ALIGN=JUSTIFY STYLE=\"text-indent: 0.49in; margin-bottom: 0.11in\">\n" +
            "<BR><BR>\n" +
            "</P>\n" +
            "<P STYLE=\"margin-bottom: 0.11in\"><BR><BR>\n" +
            "</P>\n" +
            "</BODY>\n" +
            "</HTML>";

    private View rootView;

    public InfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InfoFragment newInstance() {
        return new InfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Informatii generale");

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.info_layout, container, false);
        setupView();

        return rootView;
    }

    private void setupView() {
        final WebView webView = rootView.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
//        webView.loadData(text, "text/html; charset=utf-8", "UTF-8");
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.loadData(text, "text/html; charset=UTF-8", null);


    }

}