package com.example.sorinateza;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sorinateza.models.Market;
import com.example.sorinateza.models.MarketUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MarketsFragment extends Fragment implements MarketsAdapter.ItemClickListener {

    private RecyclerView recyclerView;
    private View root;
    private MarketsAdapter adapter;

    public MarketsFragment() {
        // Required empty public constructor
    }

    public static MarketsFragment newInstance() {
        return new MarketsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_markets, container, false);
        setupRecyclerView();
        return root;
    }

    private void setupRecyclerView() {
        if (getContext() != null)
            recyclerView = root.findViewById(R.id.marketRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MarketsAdapter(getContext(), getMarkets(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(Market market) {
        openFragment(MarketDetailFragment.newInstance(market));
    }

    private List<Market> getMarkets() {
        List<Market> marketList = new ArrayList<>();
        for (Market market : MarketUtility.getMarkets(Objects.requireNonNull(getContext()))) {
            if (!market.getDenumiremarket().isEmpty() && !market.getProducător().isEmpty()) {
                marketList.add(market);
            }
        }

        return marketList;
    }

    public void openFragment(Fragment fragment) {
        if (getActivity() != null) {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

}