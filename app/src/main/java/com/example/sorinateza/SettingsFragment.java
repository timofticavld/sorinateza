package com.example.sorinateza;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class SettingsFragment extends Fragment implements OnChartGestureListener {
    public static final String APP_PREFERENCES = "mysettings";
    public static SettingsFragment inst;
    private List<String> mLabels = new ArrayList<>();
    private View rootView;
    private BarChart chart;
    private TextView greutateTextView;
    private TextView innaltimeTextView;
    private SeekBar greutateSeekbar;
    private SeekBar innaltimeSeekbar;

    private TextView imc;
    private TextView mb;
    private TextView ne;

    public SettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingsFragment newInstance() {
        if (inst == null) {
            inst = new SettingsFragment();
        }
        return inst;
    }

    public static String calculIndiceleMaseiCorporaleString(Context context) {
        float imc = calculIndiceleMaseiCorporale(context);

        Log.d("calculIndice", calculIndiceleMaseiCorporale(context) + "");

        if (imc < 18.5) {
            return "Insuficienta ponderala";
        } else if (imc >= 18.5 && imc < 24.9) {
            return "Normalitate";
        } else if (imc >= 25.0 && imc < 29.9) {
            return "Exces ponderal";
        } else if (imc >= 30.0 && imc < 34.9) {
            return "Obezitate moderata, gr. I";
        } else if (imc >= 35.0 && imc < 39.9) {
            return "Obezitate, gr. II";
        } else {
            return "Obezitate morbinda, avansata, gr. III";
        }
    }

    static int getInnaltime(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        return sharedpreferences.getInt("innaltimea", 120);
    }

    static int getGreutate(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        return sharedpreferences.getInt("greutatea", 75);
    }

    public static float calculIndiceleMaseiCorporale(Context context) {
        float imc = ((float)getGreutate(context) / (((float)getInnaltime(context) / 100) * 2));
        return imc;
    }

    public static int calculMetabolismBazal(Context context) {
        double MB = 0.0;
        SharedPreferences sharedpreferences = context.getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        int age = sharedpreferences.getInt("age", 0);

        if (getSex(context) == 0) {
            MB = 66.473 +
                    (9.563 * getGreutate(context)) +
                    (1.8496 * getInnaltime(context)) -
                    (4.6756 * age);
        } else {
            MB = 655.095 +
                    (9.563 * getGreutate(context)) +
                    (1.8496 * getInnaltime(context)) -
                    (4.6756 * age);
        }

        return (int)MB;
    }

    public static int calculNecesarulEnergetic(Context context) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        SharedPreferences sharedpreferences = context.getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        int type = sharedpreferences.getInt(String.valueOf(mYear+mMonth+mDay), 0);
        double coef = 0.0;

        if (type == 0) {
            coef = 1.0;
        } else if (type == 1) {
            coef = 1.5;
        } else if (type == 2) {
            coef = 2.5;
        } else if (type == 3) {
            coef = 5.0;
        } else if (type == 4) {
            coef = 7.0;
        }

        double ne = calculMetabolismBazal(context) * coef;
        return (int)ne;
    }

    private void makeDays() {
        for (int i = -7; i <= 0; i++) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, i);
            mLabels.add(cal.get(Calendar.DAY_OF_MONTH) + "");
        }
    }

    private String getLabel(int i) {
        return mLabels.get(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("Setari");

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.settings_fragment, container, false);

        greutateTextView = rootView.findViewById(R.id.greutatea);
        innaltimeTextView = rootView.findViewById(R.id.innaltimea);
        greutateSeekbar = rootView.findViewById(R.id.seekBarGreutate);
        innaltimeSeekbar = rootView.findViewById(R.id.seekBarInaltime);

        imc = rootView.findViewById(R.id.imc);
        mb = rootView.findViewById(R.id.mb);
        ne = rootView.findViewById(R.id.ne);

        makeDays();
        setupSeekListener();
        updateView();

        chart = new BarChart(getActivity());
        chart.getDescription().setEnabled(false);
        chart.setOnChartGestureListener(this);

        MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv);

        chart.setDrawGridBackground(false);
        chart.setDrawBarShadow(false);


        chart.setData(generateBarData(1, 7));
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.getAxisRight().setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(false);

        // programmatically add the chart
        FrameLayout parent = rootView.findViewById(R.id.parentLayout);
        parent.addView(chart);


        return rootView;
    }

    protected BarData generateBarData(int dataSets, int count) {

        ArrayList<IBarDataSet> sets = new ArrayList<>();

        for(int i = 0; i < dataSets; i++) {

            ArrayList<BarEntry> entries = new ArrayList<>();

            for(int j = count; j >= 0; j--) {
                final Calendar c = Calendar.getInstance();
                c.add(Calendar.DATE, -j);

                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                SharedPreferences sharedpreferences = getActivity().getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
                int call = sharedpreferences.getInt(((""+mYear) + (mMonth + "") + ("" + mDay)) + "_day", 0);

                entries.add(new BarEntry(j, (float) call));
            }

            BarDataSet ds = new BarDataSet(entries, "");
            ds.setColors(ColorTemplate.VORDIPLOM_COLORS);
            sets.add(ds);
        }

        BarData d = new BarData(sets);
        return d;
    }

    private void savePreferences() {
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("innaltimea", innaltimeSeekbar.getProgress());
        editor.putInt("greutatea", greutateSeekbar.getProgress());
        editor.apply();
    }

    private void setupSeekListener() {

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);

        if (sharedpreferences.getInt("innaltimea", 0) != 0) {
            innaltimeSeekbar.setProgress(sharedpreferences.getInt("innaltimea", 0));
        }

        if (sharedpreferences.getInt("greutatea", 0) != 0) {
            greutateSeekbar.setProgress(sharedpreferences.getInt("greutatea", 0));
        }

        greutateSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateView();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        innaltimeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateView();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void updateView() {
        greutateTextView.setText(greutateSeekbar.getProgress() + "");
        innaltimeTextView.setText(innaltimeSeekbar.getProgress() + "");

        Log.d("TAG1", SettingsFragment.calculIndiceleMaseiCorporaleString(getContext()) + "");
        imc.setText(SettingsFragment.calculIndiceleMaseiCorporaleString(getContext()) + "");
        mb.setText(SettingsFragment.calculMetabolismBazal(getContext()) + "");
        ne.setText(SettingsFragment.calculNecesarulEnergetic(getContext()) + "");

        savePreferences();
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START");
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END");
        chart.highlightValues(null);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart long pressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart fling. VelocityX: " + velocityX + ", VelocityY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    private static int getSex(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        return sharedpreferences.getInt("sex", 0);
    }

}