package com.example.sorinateza.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Market implements Parcelable {

    @SerializedName("DenumireProdus")
    String denumireprodus;

    @SerializedName("producator")
    String producător;

    @SerializedName("Cantitate")
    String cantitate;

    @SerializedName("PROTEINE")
    String pROTEINE;

    @SerializedName("GRASIMI")
    String gRĂSIMI;

    @SerializedName("Vitamine")
    String vitamine;

    @SerializedName("ElementEminerale")
    String elementeminerale;

    @SerializedName("DenumireMarket")
    String denumiremarket;

    @SerializedName("Pret")
    String preț;

    @SerializedName("GLUCIDE")
    String gLUCIDE;

    @SerializedName("ValoareaEnergetica")
    String valoareaenergetică;

    @SerializedName("Fibre")
    String fibre;

    protected Market(Parcel in) {
        denumireprodus = in.readString();
        producător = in.readString();
        cantitate = in.readString();
        pROTEINE = in.readString();
        gRĂSIMI = in.readString();
        vitamine = in.readString();
        elementeminerale = in.readString();
        denumiremarket = in.readString();
        preț = in.readString();
        gLUCIDE = in.readString();
        valoareaenergetică = in.readString();
        fibre = in.readString();
    }

    public static final Creator<Market> CREATOR = new Creator<Market>() {
        @Override
        public Market createFromParcel(Parcel in) {
            return new Market(in);
        }

        @Override
        public Market[] newArray(int size) {
            return new Market[size];
        }
    };

    public String getDenumireprodus() {
        return this.denumireprodus;
    }

    public void setDenumireprodus(String denumireprodus) {
        this.denumireprodus = denumireprodus;
    }

    public String getProducător() {
        return this.producător != null ? this.producător : "";
    }

    public void setProducător(String producător) {
        this.producător = producător;
    }

    public String getCantitate() {
        return this.cantitate != null ? this.cantitate : "Necunoscut";
    }

    public void setCantitate(String cantitate) {
        this.cantitate = cantitate;
    }

    public String getPROTEINE() {
        return this.pROTEINE != null ? this.pROTEINE : "Necunoscut";
    }

    public void setPROTEINE(String pROTEINE) {
        this.pROTEINE = pROTEINE;
    }

    public String getGRĂSIMI() {
        return this.gRĂSIMI != null ? this.gRĂSIMI : "Necunoscut";
    }

    public void setGRĂSIMI(String gRĂSIMI) {
        this.gRĂSIMI = gRĂSIMI;
    }

    public String getVitamine() {
        return this.vitamine != null ? this.vitamine : "Necunoscut";
    }

    public void setVitamine(String vitamine) {
        this.vitamine = vitamine;
    }

    public String getElementeminerale() {
        return this.elementeminerale != null ? this.elementeminerale : "Necunoscut";
    }

    public void setElementeminerale(String elementeminerale) {
        this.elementeminerale = elementeminerale;
    }

    public String getDenumiremarket() {
        return this.denumiremarket != null ? this.denumiremarket : "Necunoscut";
    }

    public void setDenumiremarket(String denumiremarket) {
        this.denumiremarket = denumiremarket;
    }

    public String getPreț() {
        return this.preț != null ? this.preț :"Necunoscut" ;
    }

    public void setPreț(String preț) {
        this.preț = preț;
    }

    public String getGLUCIDE() {
        return this.gLUCIDE != null ? this.gLUCIDE : "Necunoscut";
    }

    public void setGLUCIDE(String gLUCIDE) {
        this.gLUCIDE = gLUCIDE;
    }

    public String getValoareaenergetică() {
        return this.valoareaenergetică != null ? this.valoareaenergetică : "Necunoscut";
    }

    public void setValoareaenergetică(String valoareaenergetică) {
        this.valoareaenergetică = valoareaenergetică;
    }

    public String getFibre() {
        return this.fibre;
    }

    public void setFibre(String fibre) {
        this.fibre = fibre != null ? this.fibre  : "Necunoscut";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(denumireprodus);
        parcel.writeString(producător);
        parcel.writeString(cantitate);
        parcel.writeString(pROTEINE);
        parcel.writeString(gRĂSIMI);
        parcel.writeString(vitamine);
        parcel.writeString(elementeminerale);
        parcel.writeString(denumiremarket);
        parcel.writeString(preț);
        parcel.writeString(gLUCIDE);
        parcel.writeString(valoareaenergetică);
        parcel.writeString(fibre);
    }

    public boolean isSection() {
        return (getProducător().isEmpty());
    }
}

