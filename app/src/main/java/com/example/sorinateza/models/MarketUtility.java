package com.example.sorinateza.models;

import android.content.Context;
import android.util.ArrayMap;

import com.example.sorinateza.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MarketUtility {
    private static Map<String, List<Market>> listMapMarkets = new ArrayMap<>();

    private static String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            String json = new String(bytes);
            return json;
        } catch (IOException e) {
            return null;
        }
    }

    public static List<Market> getMarkets(Context context) {
        Gson gson = new Gson();
        List<Market> markets = new ArrayList<>();
        String myJson = inputStreamToString(context.getResources().openRawResource(R.raw.market));

        Market[] data = gson.fromJson(myJson, Market[].class);

        for (Market market : data) {
            System.out.println(market);
            markets.add(market);
        }

        return markets;
    }


    public static Map<String, List<Market>> getListMapMarkets(Context context) {
        listMapMarkets.clear();
        List<Market> markets = getMarkets(context);
        List<Market> newMarkets = new ArrayList<>();

        String key = null;
        for (Market market:
                markets) {

            if (market.isSection()) {
                if (!newMarkets.isEmpty() && key != null)  {
                    listMapMarkets.put(key, newMarkets);
                    newMarkets = new ArrayList<>();
                }

                key = market.getDenumireprodus();

            } else {
                newMarkets.add(market);
            }
        }

        return listMapMarkets;
    }

}
