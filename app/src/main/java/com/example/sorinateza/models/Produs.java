package com.example.sorinateza.models;

import com.google.gson.annotations.SerializedName;

public class Produs {
    @SerializedName("Produsul")
    String produsul;

    @SerializedName("Proteine")
    String proteine;

    @SerializedName("Lipide")
    String lipide;

    @SerializedName("Glucide")
    String glucide;

    @SerializedName("Ca")
    String ca;

    @SerializedName("Fe")
    String fe;

    @SerializedName("K")
    String k;

    @SerializedName("A")
    String a;

    @SerializedName("E")
    String e;

    @SerializedName("D")
    String d;

    @SerializedName("B9")
    String b9;

    @SerializedName("B12")
    String b12;

    private int cantitateGrame;

    private boolean isChecked = false;

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public String getProdusul() {
        return this.produsul;
    }

    public void setProdusul(String produsul) {
        this.produsul = produsul;
    }

    public String getProteine() {
        return this.proteine;
    }

    public void setProteine(String proteine) {
        this.proteine = proteine;
    }

    public String getLipide() {
        return this.lipide;
    }

    public void setLipide(String lipide) {
        this.lipide = lipide;
    }

    public String getGlucide() {
        return this.glucide;
    }

    public void setGlucide(String glucide) {
        this.glucide = glucide;
    }

    public String getCa() {
        return this.ca;
    }

    public void setCa(String ca) {
        this.ca = ca;
    }

    public String getFe() {
        return this.fe;
    }

    public void setFe(String fe) {
        this.fe = fe;
    }

    public String getK() {
        return this.k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public String getA() {
        return this.a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getE() {
        return this.e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getD() {
        return this.d;
    }

    public void setD(String d) {
        this.d = d;
    }

    @SerializedName("B9")
    public String getB9() {
        return this.b9;
    }

    public void setB9(String b9) {
        this.b9 = b9;
    }

    public String getB12() {
        return this.b12;
    }

    public void setB12(String b12) {
        this.b12 = b12;
    }

    public boolean isSection() {
        return getProteine().isEmpty() && getLipide().isEmpty() && getGlucide().isEmpty();
    }

    public void setCantitateGrame(int cantitateGrame) {
        this.cantitateGrame = cantitateGrame;
    }

    public int getCantitateGrame() {
        return cantitateGrame;
    }
}


