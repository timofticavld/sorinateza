package com.example.sorinateza.models;


import android.content.Context;
import android.util.ArrayMap;

import com.example.sorinateza.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProdusData {

    private static Map<String, List<Produs>> listMap = new ArrayMap<>();

    private static String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            String json = new String(bytes);
            return json;
        } catch (IOException e) {
            return null;
        }
    }

    public static List<Produs> getProduse(Context context) {
        Gson gson = new Gson();
        List<Produs> produses = new ArrayList<>();
        String myJson = inputStreamToString(context.getResources().openRawResource(R.raw.baza));

        Produs[] data = gson.fromJson(myJson, Produs[].class);

        for (Produs model : data) {
            System.out.println(model);
            produses.add(model);
        }

        return produses;
    }

    public static Map<String, List<Produs>> getListMapProduse(Context context) {
        listMap.clear();
        List<Produs> produses = getProduse(context);
        List<Produs> newProduses = new ArrayList<>();

        String key = null;
        for (Produs prod:
             produses) {

            if (prod.isSection()) {
                if (!newProduses.isEmpty() && key != null)  {
                    listMap.put(key, newProduses);
                    newProduses = new ArrayList<>();
                }

                key = prod.getProdusul();

            } else {
                newProduses.add(prod);
            }
        }

        return listMap;
    }

}
